package demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pathVar")
public class PathVariableController {
    @RequestMapping("p1/{id}")
    public String p1(@PathVariable(name="id") String name, Model model) {
        String tip = "path参数，修改URL（/pathVar/p1/XXX）中的值，观察结果有什么不同";
        model.addAttribute("tip", tip);
        String msg = String.format("客户端请求 name = %s", name);
        model.addAttribute("msg", msg);
        return "controller/parameter";
    }

    @RequestMapping("p2/{year}/{month}")
    public String p2(@PathVariable(name="year") String year, @PathVariable(name="month") String month, Model model) {
        String tip = "path参数，修改URL（/pathVar/p2/year/month）中year,month的值，month可以是数字或其它字符，观察结果有什么不同";
        model.addAttribute("tip", tip);
        String msg = String.format("客户端请求 %s年%s月", year, month);
        model.addAttribute("msg", msg);
        return "controller/parameter";
    }

    @RequestMapping("p3/{year:\\d{4}}/{month:\\d{1,2}}")
    public String p3(@PathVariable(name="year") String year, @PathVariable(name="month") String month, Model model) {
        String tip = "path参数，修改URL（/pathVar/p3/year/month）中year,month的值，month只能是数字，若改为其它字符，观察结果有什么不同<br/>" +
                "把11修改为17试试看，能否通过验证，<br/>" +
                "要让月份在1-12之间要如何写正则表达式？";
        model.addAttribute("tip", tip);
        String msg = String.format("客户端请求 %s年%s月", year, month);
        model.addAttribute("msg", msg);
        return "controller/parameter";
    }
}
