package demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/viewname")
public class ViewNameController {
    @GetMapping("void")
    public void returnVoid(Model model) {
        String msg = "视图名称，隐式转换";
        model.addAttribute("msg", msg);
    }

    @GetMapping("college")
    @ModelAttribute("c")
    public College returnCollege() {
        return new College("01", "计算机科学与技术");
    }
}
