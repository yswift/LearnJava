package demo.security;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/security")
public class SecurityDemoController {
    @RequestMapping("/login-success")
    @ResponseBody
    public String loginSuccess(Authentication authentication) {
        return "欢迎：" + authentication.getName();
    }

    /**
     * 简单的显示当前登录用户信息
     * @param model model
     * @param authentication 当前登录的用户
     * @return view file
     */
    @RequestMapping(value = {"/user", "/admin"})
    public String user(Model model, Authentication authentication) {
        model.addAttribute("username", authentication.getName());
        model.addAttribute("principal", authentication.getPrincipal());
        return "security/user";
    }
}
