package demo.validation.group;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.sql.Date;

//@Table(name="\"Student\"")
public class Student {
    public interface Create {}
    public interface Update {}
    @NotBlank(message = "Id 不能为空", groups = { Update.class })
    private String id;

    @NotBlank(message = "学号不能为空", groups = { Create.class, Update.class })
    @Pattern(regexp = "^20\\d{10}$", message = "20开头的12位数字")
    private String no;

    @NotBlank(message = "姓名不能为空", groups = { Create.class, Update.class })
    @Length(min = 2, max = 20, message = "最少两个字符")
    private String name;

    @NotNull(message = "年龄不为空", groups = { Create.class, Update.class })
    @Range(min = 5, max = 60, message = "必须是5-60岁")
    private int age;

    @NotNull
    @Past(message = "必须是过去的日期", groups = { Create.class, Update.class })
    private Date birthday;
    private byte[] photo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "Student [id=" + id + ", no=" + no + ", name=" + name + ", age=" + age + ", birthday=" + birthday + "]";
    }
}
