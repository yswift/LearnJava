package demo.validation;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class ValidationTest {
    public static void main(String[] args) {
        // 验证
        Student s = new Student();
        s.setName("1");
        s.setAge(4);


        try (ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory()) {
            // validatorFactory 需要close
            Validator validator = validatorFactory.getValidator();

            // 验证
            Set<ConstraintViolation<Student>> result = validator.validate(s);

            // 对结果进行遍历输出
            result.stream().map(v -> v.getPropertyPath() + " " + v.getMessage() + ": " + v.getInvalidValue())
                    .forEach(System.out::println);
        }
    }
}
