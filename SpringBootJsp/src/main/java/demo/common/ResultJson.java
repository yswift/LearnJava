package demo.common;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class ResultJson {
    //    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    //    @ApiModelProperty(value = "返回码")
    private Integer code;

    //    @ApiModelProperty(value = "返回消息")
    private String message;

    //    @ApiModelProperty(value = "返回数据")
    private Map<String, Object> data = new HashMap<>();

    // 把构造方法私有化，外部不可创建对象
    private ResultJson() {}

    public static ResultJson ok() {
        ResultJson r = new ResultJson();
        r.setSuccess(true);
        r.setCode(HttpStatus.OK.value());
        r.setMessage("成功");
        return r;
    }

    public static ResultJson error() {
        ResultJson r = new ResultJson();
        r.setSuccess(false);
        r.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        r.setMessage("失败");
        return r;
    }

    public ResultJson message(String message) {
        this.setMessage(message);
        return this;
    }

    public ResultJson code(HttpStatus code) {
        this.setCode(code.value());
        return this;
    }

    public ResultJson code(Integer code) {
        this.setCode(code);
        return this;
    }

    public ResultJson data(String key, Object value) {
        this.data.put(key, value);
        return this;
    }

    public ResultJson data(Map<String, Object> map) {
        this.setData(map);
        return this;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
