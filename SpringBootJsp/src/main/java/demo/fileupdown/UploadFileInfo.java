package demo.fileupdown;

public class UploadFileInfo {
    String name;
    String mime;
    long  size;
    byte[] data;

    @Override
    public String toString() {
        return "UploadFileInfo{" +
                "name='" + name + '\'' +
                ", mime='" + mime + '\'' +
                ", size=" + size +
                '}';
    }
}
