package demo.error;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ErrorDemoController {
    @ResponseBody // 直接返回字符串，不用jsp文件
    @RequestMapping("/error-demo")
    public String testException(String data) {
        // 当 data == null, NullPointerException
        String str = data.substring(0);

        // 当 data 不是数字，NumberFormatException: For input string: "abc"
        int a = Integer.parseInt(data);

        // 当 data = 0, ArithmeticException: / by zero
        int b = 1 / a;

        // 当 data = 1，StringIndexOutOfBoundsException: begin 0, end 2, length 1
        str = data.substring(0, 2);

        return "无异常发生";
    }
}
