<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
  <title>数据验证-分组</title>
  <link rel="stylesheet" href="/static/bootstrap.css">
  <script src="/static/jquery-3.4.0.min.js"></script>
  <script src="/static/popper.min.js"></script>
  <script src="/static/bootstrap.js"></script>
</head>
<body>
<%--学生信息表单--%>
<div class="container">
  <h3>分组验证</h3>
  <form:form method="post" modelAttribute="student">
    <div class="form-group row">
      <form:label path="id" cssClass="col-md-2 col-form-label">Id</form:label>
      <div class="col-md-10">
        <form:input path="id" cssClass="form-control"/>
        <form:errors path="id" cssStyle="color:red"/>
      </div>
    </div>
    <div class="form-group row">
      <form:label path="no" cssClass="col-md-2 col-form-label">学号</form:label>
      <div class="col-md-10">
        <form:input path="no" cssClass="form-control"/>
        <form:errors path="no" cssStyle="color:red"/>
      </div>
    </div>
    <div class="form-group row">
      <form:label path="name" cssClass="col-md-2 col-form-label">姓名</form:label>
      <div class="col-md-10">
        <form:input path="name" cssClass="form-control"/>
        <form:errors path="name" cssStyle="color:red"/>
      </div>
    </div>
    <div class="form-group row">
      <form:label path="age" cssClass="col-md-2 col-form-label">年龄</form:label>
      <div class="col-md-10">
        <form:input path="age" cssClass="form-control"/>
        <form:errors path="age" cssStyle="color:red"/>
      </div>
    </div>
    <div class="form-group row">
      <form:label path="birthday" cssClass="col-md-2 col-form-label">出生日期</form:label>
      <div class="col-md-10">
        <form:input path="birthday"  cssClass="form-control"/>
        <form:errors path="birthday" cssStyle="color:red"/>
      </div>
    </div>
    <div class="form-group row">
      <div class="col-md-10 offset-md-2">
        <input type="button" class="btn btn-success" onclick="btn_click('/validate-group-create')" value="新建">
        <input type="button" class="btn btn-success" onclick="btn_click('/validate-group-edit')" value="修改">
      </div>
    </div>
  </form:form>
</div>

</body>
<script>
  function btn_click(action) {
    var f = document.getElementById("student");
    f.action = action;
    f.submit();
  }
</script>
</html>
