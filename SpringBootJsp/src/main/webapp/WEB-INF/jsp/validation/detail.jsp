<%@ page import="demo.validation.Student" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生信息</title>
    <link rel="stylesheet" href="/static/bootstrap.css">
    <script src="/static/jquery-3.4.0.min.js"></script>
    <script src="/static/popper.min.js"></script>
    <script src="/static/bootstrap.js"></script>
</head>
<body>
<div class="container">
    <h3>学生信息</h3>
    <div class="row">
        <div class="col-8">
            <ul>
                <li>学号：${student.no}</li>
                <li>姓名：${student.name}</li>
                <li>年龄：${student.age}</li>
                <li>出生日期：${student.birthday}</li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>
