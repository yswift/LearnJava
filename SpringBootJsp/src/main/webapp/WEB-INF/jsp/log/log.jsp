<%--
  Created by IntelliJ IDEA.
  User: yanli
  Date: 2022/11/9
  Time: 8:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>日志</title>
</head>
<body>
<h3>修改配置文件application.properties中的日志级别，查看日志输出的变化</h3>
<div>修改 logging.level.root=info </div>
<ul>
    <li>TRACE</li>
    <li>DEBUG</li>
    <li>INFO</li>
    <li>WARN</li>
    <li>ERROR</li>
</ul>
</body>
</html>
