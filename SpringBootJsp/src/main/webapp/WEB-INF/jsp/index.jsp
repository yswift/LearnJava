<%--
  Created by IntelliJ IDEA.
  User: yanli
  Date: 2022/11/8
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Springboot jsp 代码</title>
</head>
<body>
Spring Boot JSP 运行成功
<h3>Controller方法返回值</h3>
<ul>
    <li><a href="returnValue/view-name" target="_blank">返回视图名，用指定视图显示</a></li>
    <li><a href="returnValue/string" target="_blank">普通字符串</a></li>
    <li><a href="returnValue/json1" target="_blank">返回College对象，转为json，发送到客户端</a></li>
    <li><a href="returnValue/json2" target="_blank">返回ResponseEntity<College>对象，转为json，发送到客户端</a></li>
    <li><a href="returnValue/file" download="a.txt">返回ResponseEntity<byte[]>对象，文件下载</a></li>
    <li><a href="returnValue/redirect" target="_blank">重定向：redirect:json1</a></li>
    <li><a href="returnValue/redirect2" target="_blank">重定向：RedirectView("json1", true)</a></li>
</ul>
<h3>视图名称隐式转换</h3>
<ul>
    <li><a href="viewname/void" target="_blank">返回void</a></li>
    <li><a href="viewname/college" target="_blank">返回College对象</a></li>
</ul>
<h3>传递数据到视图</h3>
<ul>
    <li><a href="data2view/model" target="_blank">Model model</a></li>
    <li><a href="data2view/modelMap" target="_blank">ModelMap</a></li>
    <li><a href="data2view/modelAndView" target="_blank">ModelAndView</a></li>
</ul>
<h3>请求参数 @RequestParam参数</h3>
<ul>
    <li><a href="request/p1" target="_blank">p1接收参数 name，参数可选</a></li>
    <li><a href="request/p1?name=李四" target="_blank">p1接收参数 name=李四</a></li>
    <li><a href="request/p2?name=王五" target="_blank">p2接收参数 必须提供name</a></li>
    <li><a href="request/p3?req=王五五" target="_blank">p3客户端提供的参数和形参的名称不一样</a></li>
    <li><a href="request/p4" target="_blank">p4参数默认值</a></li>
    <li><a href="request/p4?req=aaa" target="_blank">p4参数默认值，req=aaa</a></li>
    <li><a href="request/p5?id=01&name=工学院" target="_blank">p5整体参数,p5?id=01&name=工学院</a></li>
</ul>
<h3>Url path 参数 @PathVariable</h3>
<ul>
    <li><a href="pathVar/p1/张三" target="_blank">/pathVar/p1/张三</a></li>
    <li><a href="pathVar/p2/2020/07" target="_blank">/pathVar/p2/2020/07</a></li>
    <li><a href="pathVar/p3/2021/11" target="_blank">带参数验证 /pathVar/p3/2020/11</a></li>
</ul>
<h3>数据验证</h3>
<ul>
    <li><a href="validate-student/" target="_blank">数据验证</a></li>
    <li><a href="validate-group/" target="_blank">分组验证</a></li>
</ul>
<h3>日志</h3>
<ul>
    <li><a href="log" target="_blank">日志</a></li>
</ul>
<h3>异常处理</h3>
<ul>
    <li><a href="error-demo/?data=123" target="_blank">无异常</a></li>
    <li><a href="error-demo/" target="_blank">NullPointerException</a></li>
    <li><a href="error-demo/?data=abc" target="_blank">NumberFormatException: For input string: "abc"</a></li>
    <li><a href="error-demo/?data=0" target="_blank">ArithmeticException: / by zero，自定义视图</a></li>
    <li><a href="error-demo/?data=1" target="_blank">StringIndexOutOfBoundsException: begin 0, end 2, length 1，json格式输出</a></li>
</ul>
<h3>文件上传、下载</h3>
<ul>
    <li><a href="file-upload/" target="_blank">文件上传、下载</a></li>
</ul>
<h3>数据库增删改查</h3>
<ul>
    <li><a href="student/list" target="_blank">学生表 增删改查</a></li>
</ul>
<h3>权限控制</h3>
<ul>
    <li>内建用户：user/123456</li>
    <li>内建用户：admin/123456</li>
    <li><a href="/login" target="_blank">登录</a></li>
    <li><a href="/logout">退出</a></li>
    <li><a href="/security/user" target="_blank">登录后能访问的内容</a></li>
    <li><a href="/security/admin" target="_blank">Admin能访问的内容</a></li>
</ul>
</body>
</html>
