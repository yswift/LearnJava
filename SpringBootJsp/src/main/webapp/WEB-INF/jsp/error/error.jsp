<%@ page import="org.springframework.web.context.request.WebRequest" %>
<%@ page import="org.aspectj.weaver.ast.Var" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="java.io.BufferedWriter" %>
<%@ page import="java.io.StringWriter" %>
<%@ page import="java.io.PrintWriter" %><%--
  Created by IntelliJ IDEA.
  User: yswif
  Date: 2022/11/10
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>自定义错误处理</title>
</head>
<body>
<%
    Exception e = (Exception) request.getAttribute("exception");
%>
<%--<%--%>
<%--    Enumeration<String> as = request.getAttributeNames();--%>
<%--    while(as.hasMoreElements()) {--%>
<%--        String a = as.nextElement();--%>
<%--        out.println(a + "=" + request.getAttribute(a) + "<br/>");--%>
<%--    }--%>
<%--%>--%>
<dl>
    <dt>path</dt>
    <dd><%= request.getAttribute("javax.servlet.forward.request_uri") %></dd>

    <dt>请求参数</dt>
    <dd><%= request.getAttribute("javax.servlet.forward.query_string") %></dd>

    <dt>异常</dt>
    <dd><%= e %></dd>

    <dt>异常堆栈</dt>
    <dd><code><pre>
        <% e.printStackTrace(new java.io.PrintWriter(out)); %>
    </pre></code>
    </dd>
</dl>

</body>
</html>
