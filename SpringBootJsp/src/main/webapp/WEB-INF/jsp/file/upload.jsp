<%--
  Created by IntelliJ IDEA.
  User: yanli
  Date: 2022/11/14
  Time: 22:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>上传文件</title>
</head>
<body>
<form method="post" enctype="multipart/form-data">
  <div class="form-group row">
    <label for="photofile" class="col-md-2 col-form-label">照片</label>
    <div class="col-md-10">
      <input type="file" class="form-control" id="photofile" name="photofile">
    </div>
  </div>
  <div class="form-group row">
    <div class="col-md-10 offset-md-2">
      <input type="submit" class="btn btn-success" value="保存">
    </div>
  </div>
</form>
</body>
</html>
