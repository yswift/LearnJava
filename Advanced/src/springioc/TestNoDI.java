package springioc;

public class TestNoDI {
    public static void main(String[] args) {
        TextEditor textEditor = new TextEditor();
        textEditor.setSpellChecker(new SpellChecker());
        textEditor.spellCheck();
    }
}
