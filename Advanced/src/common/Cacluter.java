package common;

import java.util.*;

public class Cacluter {
    public interface MathOperation {
        double op(double a, double b);
    }
    private static Map<String, MathOperation> mathOperationMap = new HashMap<>();
    static {
        mathOperationMap.put("+", new MathOperation() {
            @Override
            public double op(double a, double b) {
                return a + b;
            }
        });
        mathOperationMap.put("-", (a, b) -> { return a-b; });
        mathOperationMap.put("*", (a, b) -> a*b);
        mathOperationMap.put("/", (a, b) -> a/b);
        mathOperationMap.put("%", (a, b) -> a%b);
        // 作业，加计算乘方的运算符 ^
    }

    public static void main(String[] args) {
        System.out.println("输入算式，运算符，运算数用空格分割：");
        Scanner in = new Scanner(System.in);
        double a = in.nextDouble();
        String op = in.next();
        double b = in.nextDouble();
        MathOperation mop = mathOperationMap.get(op);
        if (mop == null) {
            System.out.println("不支持的运算符:" + op);
            return;
        }
        double r = mop.op(a, b);
        System.out.println(a + op + b + " = " + r);
    }


}
