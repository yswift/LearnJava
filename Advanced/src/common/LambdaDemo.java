package common;

import java.util.function.Function;

public class LambdaDemo {
    public static void main(String[] args) {
        Function<Integer, Double> f1 = (a) -> { return 1.0*a*a; };
        Function<Integer, Double> f2 = (a) -> { return Math.sqrt(a); };

        ff(10, f1);
        ff(5, f1);
        ff(10, f2);
        ff(7, f2);
    }

    static void ff(int v, Function<Integer, Double> f) {
        double r = f.apply(v);
        System.out.println("r = " + r);
    }
}
