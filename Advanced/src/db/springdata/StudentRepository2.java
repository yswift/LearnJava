package db.springdata;

import db.model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.awt.print.Pageable;
import java.util.Date;
import java.util.List;

// 实验4接口
public interface StudentRepository2 extends JpaRepository<Student, Integer> {
    // 定义年龄小于给定参数的方法
    List<Student> findAllByAgeBefore(int age);

    // 定义出生日期在给定参数期间的方法
    List<Student> findAllByBirthdayBetween(Date b, Date e);

    // 定义按年龄排序的方法
    List<Student> findAllByOrderByAge();

    // 定义按出生日期排序,且有分页功能的方法
}
