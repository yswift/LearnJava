package db.springdata;

import db.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class TestStudentRepository2 {
    @Autowired
    StudentRepository2 repository;

    void output(Iterable<Student> students) {
        for (Student s : students) {
            System.out.println(s);
        }
    }

    // 查找年龄小于15的学生
    void listByAge() {
        List<Student> l1 = repository.findAllByAgeBefore(15);
        System.out.println("查找年龄小于15的学生");
        output(l1);
    }

    // 查找出生日期范围的学生
    void listBirthdayBetween() throws ParseException {
        // 用两种方法获取日期对象
        // 1) parse String to date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date b = sdf.parse("2010-01-01");
        // 2) 使用 calendar
        Calendar c = Calendar.getInstance();
        c.set(2015, Calendar.MAY, 1);
        Date e = c.getTime();

        List<Student> l2 = repository.findAllByBirthdayBetween(b, e);
        System.out.println("查找出生日期范围的学生");
        output(l2);
    }

    // 按年龄排序
    void listOrderByAge() {
        List<Student> l3 = repository.findAllByOrderByAge();
        System.out.println("按年龄排序");
        output(l3);
    }

    // 分页、排序
    void listPage(Pageable pageable) {
        Page<Student> l4 = repository.findAll(pageable);
        System.out.println("分页");
        System.out.println("总页数：" + l4.getTotalPages());
        System.out.println("总元素：" + l4.getTotalElements());
        output(l4);
    }

    public static void main(String[] args) throws ParseException {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig4H2.class);
        TestStudentRepository2 t = ctx.getBean(TestStudentRepository2.class);

        // 查找年龄小于15的学生
        t.listByAge();

        // 查找出生日期范围的学生
        t.listBirthdayBetween();

        // 按年龄排序
        t.listOrderByAge();

        // 仅分页
        Pageable pageable = PageRequest.of(0, 10);
        t.listPage(pageable);

        // 分页+排序
        Sort s = Sort.by(Sort.Order.by("birthday"));
        pageable = PageRequest.of(0, 10, s);
        t.listPage(pageable);
    }
}
