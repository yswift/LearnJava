package db.springdata;

import db.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Component
public class Student4JPA {
    @Autowired
    StudentRepository studentRepository;

    // 插入student对象到表中
    public void insert(Student student) throws SQLException {
        studentRepository.save(student);
    }

    // 删除指定id的记录
    public void delete(int id) throws SQLException {

    }

    // 修改表数据，类似insert
    public void update(Student student) throws SQLException {

    }

    // 按 id 查询学生信息，把查到的数据保存到Student对象中，
    // 参考：StudentDAO 的 createStudent 方法
    public Student findById(int id) throws SQLException {
        return new Student();
    }
}
