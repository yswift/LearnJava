package db.jdbc;

import db.h2.H2DbHelper;
import io.txtfile.TxtFile;
import org.junit.Test;

import javax.sql.rowset.serial.SerialBlob;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;

public class StudentExercise {
    Connection connection;

    // 构造函数，传入数据库连接
    public StudentExercise(Connection connection) {
        this.connection = connection;
    }

    // 用给定的no学号，name姓名，age年龄，birthday出生日期，插入到表中
    public void insert(String no, String name, int age, Date birthday) throws SQLException {
    }

    // 删除指定id的记录
    public void delete(int id) throws SQLException {
    }

    // 修改表数据，按id修改数据，类似insert
    public void update(int id, String no, String name, int age, Date birthday) throws SQLException {
    }

    // 按 id 查询学生信息，把查到的数据保存到Student对象中，
    // 参考：StudentDAO 的 createStudent 方法
    public Student findById(int id) throws SQLException {
        return new Student();
    }

    public static void insertPhoto() throws IOException, SQLException {
        // 插入数据时个不好编写测试，各位同学完成后，可以使用同时插入图片，这
        // StudentView 验证
        // 方法：
        // 1. 读取图片文件到 byte[]
        String fn = StudentExercise.class.getClassLoader().getResource("photo.jpg").getFile();
        File photoFile = new File(fn);
        byte[] data = Files.readAllBytes(Path.of(photoFile.getAbsolutePath()));
        // 2. 创建 SerialBlob
        SerialBlob photoBlob = new SerialBlob(data);
        // 3. 更新数据库表
        String sql = "update Student set Photo=? where Id=?";
        H2DbHelper dbHelper = new H2DbHelper();
        try(PreparedStatement pstmt = dbHelper.getConnection().prepareStatement(sql)) {
            pstmt.setBlob(1, photoBlob);
            pstmt.setInt(2, 5);
            pstmt.execute();
        }
    }

    public static void main(String[] args) throws SQLException, IOException {
        insertPhoto();
    }
}
