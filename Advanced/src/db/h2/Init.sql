create table IF NOT EXISTS College(
    Id varchar(10) not null,
    Name varchar(50) not null,
    constraint College_pk
        primary key (Id)
);
delete from College;
insert into College values('00','计科');
insert into College values('01','通信');

create table IF NOT EXISTS Student
(
    Id int auto_increment
        primary key,
    No varchar(50),
    Name varchar(50),
    Age int null,
    Birthday date null,
    Photo blob null
);
delete from Student;
insert into Student values(null, '201187654321', '憨豆', 67, '1955-01-06', null);
insert into Student values(null, '2022001', 'Name2022001', 20, '2002-08-04', null);
insert into Student values(null, '2022002', 'Name2022002', 14, '2008-09-08', null);
insert into Student values(null, '2022003', 'Name2022003', 18, '2004-07-11', null);
insert into Student values(null, '2022004', 'Name2022004', 19, '2003-07-19', null);
insert into Student values(null, '2022005', 'Name2022005', 19, '2003-10-10', null);
insert into Student values(null, '2022006', 'Name2022006', 12, '2010-08-13', null);
insert into Student values(null, '2022007', 'Name2022007', 14, '2008-08-28', null);
insert into Student values(null, '2022008', 'Name2022008', 13, '2009-08-28', null);
insert into Student values(null, '2022009', 'Name2022009', 12, '2010-08-21', null);
insert into Student values(null, '2022010', 'Name2022010', 11, '2011-08-31', null);
insert into Student values(null, '2022011', 'Name2022011', 13, '2009-08-05', null);
insert into Student values(null, '2022012', 'Name2022012', 15, '2007-08-20', null);
insert into Student values(null, '2022013', 'Name2022013', 18, '2004-09-12', null);
insert into Student values(null, '2022014', 'Name2022014', 12, '2010-08-04', null);
insert into Student values(null, '2022015', 'Name2022015', 13, '2009-09-01', null);
insert into Student values(null, '2022016', 'Name2022016', 19, '2003-08-25', null);
insert into Student values(null, '2022017', 'Name2022017', 17, '2005-07-25', null);
insert into Student values(null, '2022018', 'Name2022018', 13, '2009-08-01', null);
insert into Student values(null, '2022019', 'Name2022019', 19, '2003-08-15', null);
insert into Student values(null, '2022020', 'Name2022020', 11, '2011-08-02', null);
insert into Student values(null, '2022021', 'Name2022021', 15, '2007-09-21', null);
insert into Student values(null, '2022022', 'Name2022022', 15, '2007-09-30', null);
insert into Student values(null, '2022023', 'Name2022023', 15, '2007-09-18', null);
insert into Student values(null, '2022024', 'Name2022024', 18, '2004-07-31', null);
insert into Student values(null, '2022025', 'Name2022025', 10, '2012-08-04', null);
insert into Student values(null, '2022026', 'Name2022026', 11, '2011-09-13', null);
insert into Student values(null, '2022027', 'Name2022027', 11, '2011-08-29', null);
insert into Student values(null, '2022028', 'Name2022028', 20, '2002-09-04', null);
insert into Student values(null, '2022029', 'Name2022029', 15, '2007-07-09', null);
insert into Student values(null, '2022030', 'Name2022030', 14, '2008-09-11', null);




