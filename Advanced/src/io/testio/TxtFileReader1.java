package io.testio;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

public class TxtFileReader1 {
    public static String readTxtFile1(String fn, String charset) throws IOException {
        // 直接使用Reader
        try (FileInputStream fis = new FileInputStream(fn);
             InputStreamReader isr = new InputStreamReader(fis, charset)) {
            char[] buf = new char[1024];
            StringBuilder sb = new StringBuilder();
            while (true) {
                int len = isr.read(buf);
                if (len == -1) break;
                sb.append(buf, 0, len);
            }
            return sb.toString();
        }
    }

    public static String readTxtFile2(String fn, String charset) throws IOException {
        // 使用 1.7 的 java.nio.file.Files
        File file = new File(fn);
        Path path = file.toPath();
        return Files.readString(path, Charset.forName(charset));
    }
}
