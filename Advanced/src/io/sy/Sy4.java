package io.sy;

import io.txtfile.TxtFile;

import java.io.*;

public class Sy4 {
    public static void main(String[] args) throws IOException {
        f3();
    }

    public static void f1() throws IOException {
        InputStream is = new FileInputStream(TxtFile.UTF8);
        InputStreamReader isr = new InputStreamReader(is, "UTF8");
        char[] buffer = new char[1024];
        while (true) {
            int len = isr.read(buffer, 0, buffer.length);
            if (len == -1) {
                break;
            }
            String s = String.valueOf(buffer, 0, len);
            System.out.println(s);
        }
        isr.close();
        is.close();
    }

    public static void f2() throws IOException {
        try (InputStream is = new FileInputStream(TxtFile.UTF8);
            InputStreamReader isr = new InputStreamReader(is, "UTF8") ) {
            char[] buffer = new char[1024];
            while (true) {
                int len = isr.read(buffer, 0, buffer.length);
                if (len == -1) {
                    break;
                }
                String s = String.valueOf(buffer, 0, len);
                System.out.println(s);
            }
        }
    }

    // 按行读取
    public static void f3() throws IOException {
        try (InputStream is = new FileInputStream(TxtFile.UTF8);
             InputStreamReader isr = new InputStreamReader(is, "UTF8");
             BufferedReader br = new BufferedReader(isr)) {
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                System.out.println(line);
            }
        }
    }
}

