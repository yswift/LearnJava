package io.sy;

public class SY3 {
    public static void main(String[] args) {
        // 编写程序，输出所有希腊字母
        //提示：α 的unicode码是：码 \u03b1
//        char c = '\u03b1'; // c ='α'
        char c = 'Α';
        for (char i=c; i<c+24; i++) {
            System.out.println(i);
        }
    }
}
