package io.sy;

import java.io.*;

public class Sy2 {
    static String inFn = "d:\\t.txt";
    static String outFn = "d:\\t-e.xlsx";

    public static void main(String[] args) throws IOException {
        f2();
    }

    static void f1() throws IOException {
        InputStream ins = new FileInputStream(inFn);
        OutputStream outs = new FileOutputStream(outFn);
        byte[] buffer = new byte[1024];
        while (true) {
            int len = ins.read(buffer);
            if (len == -1) {
                break;
            }
            outs.write(buffer, 0, len);
        }
        ins.close();
        outs.close();
    }

    public static void f2() {
        InputStream ins = null;
        OutputStream outs = null;
        try {
            ins = new FileInputStream(inFn);
            outs = new FileOutputStream(outFn);
            byte[] buffer = new byte[1024];
            while (true) {
                int len = ins.read(buffer);
                if (len == -1) {
                    break;
                }
                outs.write(buffer, 0, len);
            }
        } catch (IOException e) {
            System.out.println("有异常");
            e.printStackTrace();
        } finally {
            if (ins != null) {
                System.out.println("close ins");
                try {
                    ins.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outs != null) {
                System.out.println("close outs");
                try {
                    outs.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static void f3() {
        // try (可自动关闭的资源） {
        try (InputStream ins = new FileInputStream(inFn);
            OutputStream outs = new FileOutputStream(outFn);) {
            byte[] buffer = new byte[1024];
            while (true) {
                int len = ins.read(buffer);
                if (len == -1) {
                    break;
                }
                outs.write(buffer, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 不用关闭
//        ins.close();
//        outs.close();
    }
}
