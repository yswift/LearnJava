package io.sy;

import java.io.File;
import java.util.Scanner;

public class Sy1 {
    public static void main(String[] args) {
        // 编写程序，输入文件夹的绝对路径，输出该文件夹的所有子文件夹。
        //提示：使用File类的list或listFiles方法
        System.out.println("输入文件夹名：");
        Scanner in = new Scanner(System.in);
        String folder = in.next();
        File file = new File(folder);
        if (! file.exists()) {
            System.out.println("文件夹不存在," + folder);
            return;
        }
        if (! file.isDirectory()) {
            System.out.println("不是文件夹," + folder);
            return;
        }
        String[] fns = file.list();
        for (String fn : fns) {
            System.out.println(fn);
        }
        // 建议：
        // 1. 使用 listFile() 方法替换
        // 2. 使用 FilenameFilter 过滤文件名，只输出 txt 文件
    }
}
