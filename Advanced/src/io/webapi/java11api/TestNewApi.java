package io.webapi.java11api;

import io.txtfile.TxtFile;
import org.junit.Test;

import javax.swing.*;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.time.Duration;

public class TestNewApi {
    @Test
    public void domo0() throws IOException, InterruptedException {
        // ①、创建HttpClient对象
        HttpClient client = HttpClient.newBuilder()//1
                // 指定HTTP协议的版本
                .version(HttpClient.Version.HTTP_2)
                // 指定重定向策略
                .followRedirects(HttpClient.Redirect.NORMAL)
                // 指定超时的时长
                .connectTimeout(Duration.ofSeconds(20))
                // 如有必要，可通过该方法指定代理服务器地址
//			.proxy(ProxySelector.of(new InetSocketAddress("proxy.crazyit.com", 80)))
                .build();
        // ②、创建HttpRequest对象
        HttpRequest request = HttpRequest.newBuilder()//2
                // 执行请求的URL
                .uri(URI.create("https://baike.baidu.com/item/%E7%AB%A5%E7%8E%B2/974254?fr=aladdin"))
                // 指定请求超时的时长
                .timeout(Duration.ofMinutes(2))
                // 指定请求头
                .header("Content-Type", "text/html")
                // 创建GET请求
                .GET()
                .build();
        // HttpResponse.BodyHandlers.ofString()指定将服务器响应转化成字符串
        HttpResponse.BodyHandler<String> bh = HttpResponse.BodyHandlers.ofString();
        // ③、发送请求，获取服务器响应
        HttpResponse<String> response = client.send(request,bh);//3
        // 获取服务器响应的状态码
        System.out.println("响应的状态码:" + response.statusCode());
        System.out.println("响应头:\n" + response.headers());
        System.out.println("响应体:" + response.body());
    }

    @Test
    public void domo1_1() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://www.baidu.com/"))
                .build();

        HttpResponse.BodyHandler<String> bodyHandler =
                HttpResponse.BodyHandlers.ofString();
        HttpResponse<String> res = client.send(request, bodyHandler);
        System.out.println(res.body());
    }

    @Test
    public void domo1() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("http://www.baidu.com/"))
                .build();

        client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println)
                .join();
    }

    @Test
    public void demo2() throws IOException, InterruptedException {
        // 设置请求参数
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://httpbin.org/ip"))
                .header("Accept", "application/json")
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }

    @Test
    public void demoPostJson() throws IOException, InterruptedException {
        // POST josn
        String json = "{\"id\": 999,\"value\": \"content\"}";
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://httpbin.org/post"))
                .header("Content-Type","application/json")
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }

    @Test
    public void demoPostFile() throws IOException, InterruptedException {
        // POST file
        String json = "{\"id\": 999,\"value\": \"content\"}";
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://httpbin.org/post"))
                .header("Content-Type", "octet-stream")
                .POST(HttpRequest.BodyPublishers.ofFile(Path.of(TxtFile.UTF8)))
                .build();

        HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());
        System.out.println(response.body());
    }

    @Test
    public void getImage() throws IOException, InterruptedException {
        String url = "https://www.baidu.com/img/bd_logo1.png";

        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();

        HttpResponse<byte[]> response = client.send(request,
                HttpResponse.BodyHandlers.ofByteArray());
        byte[] images = response.body();
        ImageIcon icon = new ImageIcon(images);
        JOptionPane.showMessageDialog(null, "",
                "Http获取图片", 0, icon);
    }
}
