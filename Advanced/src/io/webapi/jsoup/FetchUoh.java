package io.webapi.jsoup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.io.IOException;

public class FetchUoh {
    @Test
    public void fetchNews() throws IOException {
        Document doc = Jsoup.connect("https://www.uoh.edu.cn/").get();
        System.out.println(doc.title());
        Elements newsHeadlines = doc.select("#tab01 .li_2 a");
        for (Element headline : newsHeadlines) {
            System.out.printf("%s\t%s\n",
                    headline.text(), headline.absUrl("href"));
        }
    }
}
