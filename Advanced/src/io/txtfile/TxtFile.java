package io.txtfile;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;

public class TxtFile {
	public static final String GBK = getFullName("GBK.TXT");
	public static final String UTF8 = getFullName("UTF8.TXT");
	public static final String UTF16 = getFullName("UTF16.TXT");
	public static final String UTF32 = getFullName("UTF32.TXT");

	// 获取用于测试的txt文件的完整文件名.
	public static String getFullName(String fn) {
		URL url = TxtFile.class.getClassLoader().getResource(fn);
//		String fn  = URLDecoder.decode(url.getFile(), Charset.defaultCharset());
		return url.getFile();
	}

//	public static String getFullName(String fn) {
//		return System.getProperty("user.dir") + "\\src\\io\\txtfile\\" + fn;
//	}

	public static void main(String[] args) {
		File file = new File(getFullName("GBK.txt"));
		System.out.println(file.getAbsolutePath());
	}
}
