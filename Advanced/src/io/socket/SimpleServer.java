package io.socket;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

public class SimpleServer {
    public static void main(String[] args) throws IOException {
        System.out.println("监听6666端口");
        ServerSocket ss = new ServerSocket(6666);
        System.out.println("等待链接");
        Socket s = ss.accept();//establishes connection
        SocketAddress address = s.getRemoteSocketAddress();
        System.out.println("客户已链接: " + address);
        System.out.println("本地地址：" + s.getLocalSocketAddress());
        InputStream is = s.getInputStream();
        DataInputStream dis = new DataInputStream(is);
        String str = dis.readUTF();
        System.out.println("message= " + str);
        ss.close();
    }
}
