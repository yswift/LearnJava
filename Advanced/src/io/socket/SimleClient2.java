package io.socket;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class SimleClient2 {
    public static void main(String[] args) throws IOException {
        System.out.println("链接6666端口");
        Socket s = new Socket("localhost", 6666);
        System.out.println("连接上:" + s.getRemoteSocketAddress());
        System.out.println("本地地址：" + s.getLocalSocketAddress());
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        DataInputStream dis = new DataInputStream(s.getInputStream());
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.println("输入：");
            String s1 = in.nextLine();
            dout.writeUTF(s1);
            dout.flush();
            if ("stop".equals(s1)) {
                break;
            }
            String s2 = dis.readUTF();
            System.out.println("server: " + s2);
        }
        System.out.println("stop");
        s.close();
    }
}
