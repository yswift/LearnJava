package io.socket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SimleClient {
    public static void main(String[] args) throws IOException {
        System.out.println("链接6666端口");
        Socket s = new Socket("localhost", 6666);
        System.out.println("连接上:" + s.getRemoteSocketAddress());
        System.out.println("本地地址：" + s.getLocalSocketAddress());
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        dout.writeUTF("Hello Server");
        dout.flush();
        dout.close();
        s.close();
    }
}
