package io.socket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

public class SimpleServer2 {
    public static void main(String[] args) throws IOException {
        System.out.println("监听6666端口");
        ServerSocket ss = new ServerSocket(6666);
        System.out.println("等待链接");
        Socket s = ss.accept();//establishes connection

        SocketAddress address = s.getRemoteSocketAddress();
        System.out.println("客户已链接: " + address);
        System.out.println("本地地址：" + s.getLocalSocketAddress());
        InputStream is = s.getInputStream();
        DataInputStream dis = new DataInputStream(is);
        OutputStream os = s.getOutputStream();
        DataOutputStream dos = new DataOutputStream(os);
        Scanner in = new Scanner(System.in);
        while (true) {
            String str = dis.readUTF();
            System.out.println("client: " + str);
            if ("stop".equals(str)) {
                break;
            }
            System.out.println("输入：");
            String s2 = in.nextLine();
            dos.writeUTF(s2);
        }
        System.out.println("stop");
        ss.close();
    }
}
