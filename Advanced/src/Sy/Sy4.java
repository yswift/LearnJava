package Sy;

import com.google.gson.Gson;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Sy4 {
    public static String getIp() throws IOException, InterruptedException {
        String url = "http://api.rest7.com/v1/my_ip.php";
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String res = response.body();
        // res = ["39.129.167.21"]
        // ip = 39.129.167.21
        String ip = res.substring(2, res.length()-2);
        return ip;
    }

    public static CityInfo getCityInfoByIp(String ip) throws IOException, InterruptedException {
        String url = "http://api.rest7.com/v1/ip_to_city.php?ip=" + ip;
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        String json = response.body();
        // json 字符串转为 CityInfo 对象
        Gson gson = new Gson();
        CityInfo cityInfo = gson.fromJson(json, CityInfo.class);
        return cityInfo;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        String ip = getIp();
        System.out.println(ip);
        System.out.println("获取城市信息");
        CityInfo cityInfo = getCityInfoByIp("106.61.3.222");
        System.out.println("国家：" + cityInfo.getCountry());
        System.out.println("省：" + cityInfo.getRegion());
        System.out.println("城市：" + cityInfo.getCity());
        System.out.println("经度：" + cityInfo.getLng());
        System.out.println("纬度：" + cityInfo.getLat());
    }
}
