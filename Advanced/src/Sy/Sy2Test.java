package Sy;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Sy2Test {
    List<Student> studentList;

    @Before
    public void before() {
        studentList = Stream.generate(Student::new).limit(10).collect(Collectors.toList());
        System.out.println(studentList);
    }

    @Test
    public void testAgeThan20() {
        System.out.println("年龄大于20");
        List<Student> result = Sy2.filterAgeThan20(studentList);
        assertTrue(result.stream().allMatch(s -> s.age > 20));
    }

    @Test
    public void testAgeThan20_1() {
        System.out.println("年龄大于20");
        List<Student> result = Sy2.filterAgeThan20_1(studentList);
        assertTrue(result.stream().allMatch(s -> s.age > 20));
    }

    @Test
    public void testMajorIsComputer() {
        System.out.println("专业是计科的学生");
        List<Student> result = Sy2.filterMajorIsComputer(studentList);
        assertTrue(result.stream().allMatch(s -> "计科".equals(s.major)));
    }

    @Test
    public void testMap() {
        List<String> result = Sy2.mapName(studentList);
        assertEquals(studentList.size(), result.size());
        for (int i=0; i<studentList.size(); i++) {
            assertEquals(studentList.get(i).name, result.get(i));
        }
    }

    @Test
    public void testGroupByMajor() {
        Map<String, List<Student>> result = Sy2.groupByMajor(studentList);
        assertTrue(result.size() > 0);
        for (String major : result.keySet()) {
            List<Student> ss = result.get(major);
            assertTrue(ss.stream().allMatch(s -> major.equals(s.major)));
        }
    }

    @Test
    public void testGroupByAge() {
        Map<Integer, List<Student>> result = Sy2.groupByAge(studentList);
        assertTrue(result.size() > 0);
        for (Integer age : result.keySet()) {
            List<Student> ss = result.get(age);
            assertTrue(ss.stream().allMatch(s -> age.equals(s.age)));
        }
    }
}
