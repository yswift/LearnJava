package Sy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class Zy_W3 {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://hq.sinajs.cn/list=sh601006");
        URLConnection urlCon = url.openConnection();
        InputStream is = urlCon.getInputStream();
        InputStreamReader isr = new InputStreamReader(is, "GBK");
        BufferedReader br = new BufferedReader(isr);
        while (true) {
            String line = br.readLine();
            if (line == null){
                break;
            }
            System.out.println(line);
        }
        // 解析得到的字符串，建议使用正则表达式
        // 按各个数字的含义输出
        // var hq_str_sh601006="大秦铁路, 27.55, 27.25, 26.91, 27.55, 26.20, 26.91, 26.92,
        //22114263, 589824680, 4695, 26.91, 57590, 26.90, 14700, 26.89, 14300,
        //26.88, 15100, 26.87, 3100, 26.92, 8900, 26.93, 14230, 26.94, 25150, 26.95, 15220, 26.96, 2008-01-11, 15:05:32";
        //这个字符串由许多数据拼接在一起，不同含义的数据用逗号隔开了，按照程序员的思路，顺序号从0开始。
        //0：”大秦铁路”，股票名字；
        //1：”27.55″，今日开盘价；
        //2：”27.25″，昨日收盘价；
        //3：”26.91″，当前价格；
        is.close();
    }
}
