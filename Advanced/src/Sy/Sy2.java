package Sy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Sy2 {
    public static List<Student> filterAgeThan20(List<Student> list) {
        // 找出年龄大于20的学生;
        List<Student> newList = new ArrayList<>();
        for (Student s : list) {
            if (s.age > 20) {
                newList.add(s);
            }
        }
        return newList;
    }

    public static List<Student> filterAgeThan20_1(List<Student> list) {
        // 找出年龄大于20的学生;
        return list.stream().filter(s -> s.age > 20).collect(Collectors.toList());
    }

    public static List<Student> filterMajorIsComputer(List<Student> list) {
        // 找出专业是计科的学生;
        return list;
    }

    public static List<String> mapName(List<Student> list) {
        // 获取所有学生的姓名
        return new ArrayList<>();
    }

    public static Map<String, List<Student>> groupByMajor(List<Student> list) {
        // 按专业分组
        return new HashMap<>();
    }

    public static Map<Integer, List<Student>> groupByAge(List<Student> list) {
        // 按年龄分组
        return new HashMap<>();
    }
}
