package Sy;

import java.util.Random;

public class Student {
    static int id = 1;
    static String[] majors = {
            "计科","机械","电气","通信","信息安全"
    };
    // 学号
    String no;
    // 姓名
    String name;
    // 专业
    String major;
    // 年龄
    int age;

    Student() {
        Random r = new Random();
        // 使用随机值初始化类
        no = String.format("20%02d%02d%06d", 10+r.nextInt(10), r.nextInt(10), r.nextInt(200));
        name = "学生" + id++;
        major = majors[r.nextInt(majors.length)];
        age = 18 + r.nextInt(4);
    }

    @Override
    public String toString() {
        return "Student{" +
                "no='" + no + '\'' +
                ", name='" + name + '\'' +
                ", major='" + major + '\'' +
                ", age=" + age +
                '}' + '\n';
    }
}
